var Cylon = require('cylon');
var connectedTimes = 0;

// 73acb814dc464a2c9a929c3daec91561 2B-8DE0 f087c1108de0 nathanael
// ebe03eac6e12417788b7936847de6b55 2B-7A89 ee33f0097a89 brob

Cylon.config({
    logging: {
        level: 'debug'
    }
});

var noble = require('noble');
var targetPeripheral = {uuid:"f087c1108de0", peripherals:new Array()};

noble.on('stateChange', function(state){
    if (state == "poweredOn"){
        console.log("Scanning Started");
        noble.startScanning([], true);
    }
});

noble.on('discover', function(peripheral){
    if (peripheral.uuid == targetPeripheral.uuid) {
        targetPeripheral = peripheral;
        console.log("Found Peripheral");
        noble.stopScanning()
    }
});


noble.on('scanStop', function(){

    connectedTimes++;

    if ( connectedTimes === 1 ) {
        console.log("Connecting Peripheral");
        targetPeripheral.connect(function(err){
            console.log("CONNECTED 1");
        });

        setTimeout(function(){
            targetPeripheral.connect(function(err){
                console.log(targetPeripheral);
                console.log("CONNECTED 2");

                    console.log("Starting Cylon Robot...");
                    Cylon.robot({
                        connections: {
                            bluetooth: { adaptor: 'central', uuid: 'f087c1108de0', module: 'cylon-ble' }
                        },

                        devices: {
                            ollie: { driver: 'ollie' }
                        },

                        display: function(err, data) {
                            if (err) {
                                console.log("Error:", err);
                            } else {
                                console.log("Data:", data);
                            }
                        },

                        work: function(my) {

                            // The following code will only output "Putting Ollie into dev mode." and "Sending anti-DoS string."
                            my.ollie.devModeOn(function(err,data){
                                console.log( "Make it RED! ");
                                my.ollie.setRGB(0xFF0000);
                            });

                            setTimeout(function() {
                                //console.log("Make it GREEN");
                                //my.ollie.setRGB(0x00FF00);
                            },2000);
                        }
                    }).start();

            })
        }, 5000)
    }
});
