var noble = require('noble');
var targetPeripheral = {uuid:"ebe03eac6e12417788b7936847de6b55", peripherals:new Array()};

noble.on('stateChange', function(state){
    if (state == "poweredOn"){
        console.log("Scanning Started");
        noble.startScanning([], true);
    }
});

noble.on('discover', function(peripheral){
    if (peripheral.uuid == targetPeripheral.uuid) {
        targetPeripheral = peripheral
        console.log("Found Peripheral");
        console.log(targetPeripheral);
        noble.stopScanning()
    }
});


noble.on('scanStop', function(){
    console.log("Connecting Peripheral");
    targetPeripheral.connect(function(err){
        console.log("Did Connect?");
        console.log(err);
    });

    setTimeout(function(){
        targetPeripheral.connect(function(err){
            console.log("Did Connect?");
            //startCylonOllie(); //This is where you can start sending data to Ollie
        })
    }, 5000)
});